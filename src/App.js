import React, { useCallback, useState } from 'react'
import { useEffect } from 'react'
import Api from './api/Api'
import { Pagination } from './components'
import { useDispatch, useSelector } from 'react-redux'
import {
  selectCurrentPageData,
  selectPaginationCurrentPage,
  selectPaginationPagesData
} from './store/pagination/paginationSelectors'
import { changePage } from './store/pagination/paginationReducer'
import { Container, ListGroup, ListGroupItem, Spinner } from 'reactstrap'

const App = () => {

  const [totalPages, setTotalPages] = useState(0)
  const [isLoading, setIsLoading] = useState(false)

  const currentPage = useSelector(selectPaginationCurrentPage)
  const dataPages = useSelector(selectPaginationPagesData)
  const currentPageData = useSelector(selectCurrentPageData)

  const dispatch = useDispatch()

  const attemptGetDataUsers = async (selectedPage) => {
    try {
      const { data, totalPages } = await Api.getUsers(selectedPage, 10)
      setTotalPages(totalPages)
      dispatch(changePage({
        numberPage: selectedPage,
        data: data
      }))
    } catch (e) {
      console.log(e)
    }
  }

  const setDataUsers = async (selectedPage) => {
    setIsLoading(true)
    const existingPage = dataPages.find(item => item.numberPage === selectedPage)
    if (existingPage) {
      dispatch(changePage({
        numberPage: selectedPage,
        data: existingPage
      }))
      setIsLoading(false)
    } else {
      await attemptGetDataUsers(selectedPage)
      setIsLoading(false)
    }
  }

  useEffect(() => {
    (async () => {
      await attemptGetDataUsers(currentPage)
    })()
  }, [])

  const viewList = useCallback(() => (
    <ListGroup>
      {currentPageData.data.map(({ name }, index) => (
        <ListGroupItem key={index}>{name}</ListGroupItem>
      ))
      }
    </ListGroup>
  ), [currentPageData])

  return (
    <>
      <Container className="themed-container">
        {isLoading
          ? <div className={'h-75 d-flex justify-content-center align-items-center'}>
            <Spinner color="secondary"/>
          </div>
          : <div className={'h-75'}>
              {currentPageData && viewList()}
          </div>
        }
        <div>
          <Pagination
            initialPage={currentPage}
            onPageChange={({ selected: selectedPage }) => setDataUsers(selectedPage)}
            pageCount={totalPages}
          />
        </div>

      </Container>
    </>
  )
}

export default App

