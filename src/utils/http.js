import axios from 'axios';

const config = {
  http: {
    defaultRequest: {
      headers: {
        'Accept': '*/*',
        'Content-Type': 'application/json; charset=utf-8',
        'Cache-Control': 'no-cache',
      }
    },
    baseURL: 'https://api.instantwebtools.net/v1/'
  },
}

const http = axios.create({
  baseURL: `${config.http.baseURL}`,
  headers: config.http.defaultRequest.headers,
})

const beforeRequestSuccess = config => config
const beforeRequestError = error => error
const beforeResponseSuccess = response => response
const beforeResponseError = error => error

http.interceptors.request.use(beforeRequestSuccess, beforeRequestError)
http.interceptors.response.use(beforeResponseSuccess, beforeResponseError)

export {
  http
}

