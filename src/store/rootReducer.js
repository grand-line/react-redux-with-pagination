import { combineReducers } from 'redux';
import paginationReducer from './pagination/paginationReducer'

const rootReducer = combineReducers({
  pagination: paginationReducer,
})

export default rootReducer
