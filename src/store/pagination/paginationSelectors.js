import { createSelector } from 'reselect';

const selectPagination = state => state.pagination;

export const selectPaginationPagesData = createSelector(
  [selectPagination],
  pagination => pagination.pages
);

export const selectPaginationCurrentPage = createSelector(
  [selectPagination],
  pagination => pagination.currentPage
)

export const selectCurrentPageData = createSelector(
  [selectPagination],
  pagination => pagination.pages.find(item => item.numberPage === pagination.currentPage)
)

export const selectPaginationPageSize = createSelector(
  [selectPagination],
  pagination => pagination.currentPage
)
