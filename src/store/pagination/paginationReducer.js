import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  currentPage: 0,
  pages: [],
  size: 10
}

const paginationSlice = createSlice({
  name: 'pagination',
  initialState,
  reducers: {
    changePage (state, action) {
      const { numberPage, data } = action.payload
      return {
        ...state,
        currentPage: numberPage,
        pages: state.pages.find(item => item.numberPage === numberPage)
          ? [...state.pages]
          : [...state.pages,
                {
                  numberPage: numberPage,
                  data: data
                }
          ]
      }
    }
  }
})

export const { changePage } = paginationSlice.actions

export default paginationSlice.reducer
