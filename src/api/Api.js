import { http } from '../utils/http'

class Api {
  getUsers(page, size) {
    return new Promise((resolve, reject) => {
      http.get(`passenger?page=${page}&size=${size}`)
        .then(({ data }) => {
          resolve(data);
        })
        .catch(error => reject(error));
    })
  }
}

export default new Api()
